The most important thing: Please create a name of the project as related to the project functionality, nor for related libraries or vendors which you used during implementation. The name of the project should show the main goal of the project or its features.

Also, please pay attention to the project settings and provide real information about maintenance and development status, you can change these parameters at any time if you are a project owner.

<h3>Overview</h3>

To create this description I've used the next resources:

https://www.drupal.org/node/997024
https://www.drupal.org/node/7765
https://www.drupal.org/docs/develop/documenting-your-project/module-documentation-guidelines
http://drupalsun.com/lisa/2010/12/13/module-owners-how-make-your-module-description-useful

So, the good project description should have the next information on the project page:

Useful description idea: use subheadings
Subheadings make your description more easily scannable.

Here's a list of subheadings that would be awesome. I'll describe them fully below.

<ul>
  <li>Overview</li>
  <li>Features</li>
  <li>Requirements</li>
  <li>Known problems</li>
  <li>Tutorials/Documentations</li>
  <li>Dependencies</li>
  <li>Sponsored and maintained by (Credits)</li>
  <li>Restricions</li>
  <li>Recommended modules</li>
  <li>Resources</li>
</ul>


<h3>Sponsored and maintained by (Credits): </h3>

Add to this section maintainers and sponsors of the project.
Currently, I am a maintainer of the project. 
Profile link: https://www.drupal.org/u/ysamoylenko

<h3>Features</h3>

The project feature is a rightly described the project to provide an example for the colleagues.


<h3>Requirements</h3>

Links to required modules or (vendors) if them not included to the project via dependencies or requirements using Drupal mechanisms for it at any step of project downloading/installation.

Also, please include information about the required environment, for example:
<ul>
  <li><strong>Drupal 7.x </strong>(or special version === bad idea, ideally project should have the compatibility with any major version of Drupal core)</li>
  <li><strong>Drupal 8.x </strong>(or special version === bad idea, ideally project should have the compatibility with any major version of Drupal core)</li>
  <li><strong>PHP > 5.5 / > 7.0</strong> (or special version === bad idea, ideally project should have the best compatibility with any supported PHP version by Drupal major version, but it depends on the task requirements)</li>
</ul>

Required PHP extensions:
<ul>
  <li>bcmath</li>
  <li>gd</li>
  <li>xml</li>
  <li>etc... </li>
</ul>

Required external libraries/vendors/modules if it not added to dependencies of the project and will not be installed automatically:

<ul>
  <li>Library/Vendor/Module name: link</li>
  <li>Library/Vendor/Module name: link</li>
  <li>Library/Vendor/Module name: link</li>
</ul>

Also, this section should have the steps for installing these required modules or libraries with details, but if these instructions will have so many texts, then move these instructions to <strong>INSTALL.txt</strong> file and include it to the project archive.

Don't forget to add this information to this section.

For example: 
More information about the steps of installing project (theme/module) requirements you can find in the file <strong>INSTALL.txt</strong>.

<h3>Recommended modules</h3>

You should list any modules that are not explicitly required but will, if enabled, enhance the usefulness or user experience of the project.

Examples: 
Commerce 2.0 has recommended modules list:

Inline entity form,
Shipping,
Stock,
Coupons,
File downloads,
PayPal

<h3>Dependencies </h3>
If the project is a bridge or interface module is not fully functional on its own, but relies on some external service (e.g. SaaS) to be fully functional, such external dependencies should be disclosed.

Add the actual links for downloading them or add installation instructions. If they have so many texts, please add just a link to the service documentation page if it exists or adds this information to <strong>INSTALL.txt</strong>

<h3>Known problems</h3>
Known problems
Be upfront about any known problems, bugs, etc. Link to active issues regarding those problems. Tip: use [#nnn] syntax and Drupal.org will automatically link to the issues and show their current status inside the node body.

<h3>Resources: </h3>
The template for your project page has a tab labelled "Resources" near the bottom.

It will be very helpful for review the module functionality by the end user if you add the screenshots, videos, documentation and any other information which allow easying understand what your project does and which business problems it will be able to solve.

More details you can find on the page: https://www.drupal.org/node/997024


Finally, if the description is too long, please move some information to the <strong>README.txt</strong> or move this information to documentation page and add information about it to the project page. 

<strong>For example, it can look like: </strong>

To read more details about the project please follow the link: <a href="#">Documentation page.</a>

Also, you can view full project details in <strong>README.txt</strong> inside the project archive.

On the other hand, you can add your project help to the Drupal Help System UI.
Implementation examples:
https://www.drupal.org/docs/develop/documenting-your-project/module-documentation-guidelines#hook_help

P.S. If this sounds good to you...

Use this handy cut and paste skeleton to flesh out your project description.

<code><h3>Overview</h3>
<h3>Features</h3>
<h3>Requirements</h3>
<h3>Known problems</h3>
<h3>Tutorials/Documentations</h3>
<h3>Dependencies</h3>
<h3>Sponsored and maintained by:</h3>
<h3>Restricions</h3>
<h3>Recommended modules</h3>
<h3>Resources</h3>
</code>